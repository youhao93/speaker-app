INSERT INTO speakers (first_name, last_name)
VALUES  ('Speaker 1', 'Hallo achternaam'),
        ('Speaker 2', 'Gates'),
        ('Speaker 3', 'Alakija');

INSERT INTO skills (description)
VALUES  ('Java'),('Python'),('Spring');


INSERT INTO speakers_skills (speakers_id, skills_id)
VALUES  (1,1),
        (2,3),
        (3,1);


INSERT INTO speakers_review_ids (speakers_id, review_ids)
VALUES (1,1),(1,2),(1,3),(2,1);