package com.conferenceyh.conference.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;


@Entity(name = "speakers")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Speaker {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String first_name;
    private String last_name;

    @ManyToMany
    @JoinTable(
            name = "speakers_skills",
            joinColumns = @JoinColumn(name = "speakers_id"),
            inverseJoinColumns = @JoinColumn(name = "skills_id"))
    private List<Skill> skills;

    @ElementCollection
    private List<Long> review_ids;

    public Speaker(Long id,
                   String first_name,
                   String last_name,
                   List<Skill> skills, List<Long> reviews) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.skills = skills;
        this.review_ids = reviews;
    }

    public Speaker() {
    }

    public Speaker(Long id, String first_name, String last_name) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
}
