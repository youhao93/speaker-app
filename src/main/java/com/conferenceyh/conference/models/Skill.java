package com.conferenceyh.conference.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity(name = "skills")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;

    @ManyToMany
    @JsonIgnore
    @JoinTable(
            name = "speakers_skills",
            joinColumns = @JoinColumn(name = "skills_id"),
            inverseJoinColumns = @JoinColumn(name = "speakers_id"))
    private List<Speaker> speakers;



    public Skill(Long skill_id, String description) {
        this.id = skill_id;
        this.description = description;
    }

    public Skill() {

    }

    public Long getSkill_id() {
        return id;
    }

    public void setSkill_id(Long skill_id) {
        this.id = skill_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
