package com.conferenceyh.conference.exception;

public class SpeakerNotFoundException extends RuntimeException {
    public SpeakerNotFoundException(String exception) {
        super(exception);
    }
}
