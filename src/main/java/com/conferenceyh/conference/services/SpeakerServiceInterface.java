package com.conferenceyh.conference.services;

import com.conferenceyh.conference.models.Speaker;

import java.util.List;

public interface SpeakerServiceInterface {
    List<Speaker> findAll();

    Speaker findSpeaker(Long id);

    Speaker saveAndFlush(Speaker speaker);

    Speaker updateSpeaker(Long id, Speaker speaker);

    void deleteById(Long id);
}
