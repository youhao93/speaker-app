package com.conferenceyh.conference.services;

import com.conferenceyh.conference.exception.SpeakerNotFoundException;
import com.conferenceyh.conference.models.Speaker;
import com.conferenceyh.conference.repositories.SpeakerRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SpeakerService implements SpeakerServiceInterface{
    @Autowired
    private SpeakerRepository speakerRepository;

    public List<Speaker> findAll() {
        return speakerRepository.findAll();
    }

    public Speaker findSpeaker(Long id) {
        Optional<Speaker> optionalSpeaker = speakerRepository.findById(id);

        if(optionalSpeaker.isPresent())
            return optionalSpeaker.get();
        else
            throw new SpeakerNotFoundException("Speaker could not be found");
    }

    public Speaker saveAndFlush(Speaker speaker) {
        return speakerRepository.saveAndFlush(speaker);
    }


    public Speaker updateSpeaker(Long id, Speaker speaker) {
        Speaker existingSpeaker = speakerRepository.getOne(id);
        BeanUtils.copyProperties(speaker, existingSpeaker, "id");
        return speakerRepository.saveAndFlush(existingSpeaker);
    }

    public void deleteById(Long id) {
        Optional<Speaker> optionalSession = speakerRepository.findById(id);

        if(optionalSession.isPresent()) {
            speakerRepository.deleteById(id);
        } else {
            throw new SpeakerNotFoundException("Session could not be deleted, because session was not found");
        }
    }

}
