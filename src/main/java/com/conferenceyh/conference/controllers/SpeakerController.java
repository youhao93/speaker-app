package com.conferenceyh.conference.controllers;

import com.conferenceyh.conference.exception.SpeakerNotFoundException;
import com.conferenceyh.conference.models.Speaker;
import com.conferenceyh.conference.services.SpeakerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import javax.xml.bind.ValidationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/speakers")
public class SpeakerController {

    @Autowired
    private SpeakerService speakerService;

    @GetMapping
    public List<Speaker> list() {
        return speakerService.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    public ResponseEntity<Speaker> get(@PathVariable Long id) {
        try {
            return new ResponseEntity<Speaker>(speakerService.findSpeaker(id), HttpStatus.OK);
        } catch (SpeakerNotFoundException exception){
           throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Speaker not found");
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Speaker create(@RequestBody final Speaker speaker) throws ValidationException {
        if(speaker.getFirst_name() != null && speaker.getLast_name() != null)
            return speakerService.saveAndFlush(speaker);
        else throw new ValidationException("Speaker could not be created, missing values");
    }

    @PutMapping(value ="{id}")
    public ResponseEntity<Speaker> updateSession(@PathVariable Long id, @RequestBody Speaker speaker) {
        try {
            return new ResponseEntity<Speaker>(speakerService.updateSpeaker(id, speaker), HttpStatus.OK) ;
        } catch(SpeakerNotFoundException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Speaker could not be updated, because no speaker was found");
        }
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        try {
            speakerService.deleteById(id);
            return new ResponseEntity<String>("Speaker is deleted", HttpStatus.OK);
        } catch(SpeakerNotFoundException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

}
